package br.security.example.security;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/pessoas")
public class PessoaController {
	@PostMapping(value = "/salvar", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> salvar(@RequestBody Pessoa pessoa) {
		// Armazenar dados da pessoa
		return ResponseEntity
				.ok("Pessoa [" + pessoa.getNome() + ", " + pessoa.getCargo() + "] armazenada com sucesso!");
	}

	@GetMapping(value = "/buscar", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> buscar() {
		return ResponseEntity.ok("Deu certo!");
	} 

}