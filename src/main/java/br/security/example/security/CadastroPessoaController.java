package br.security.example.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class CadastroPessoaController {

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public String listar(Model model) {
		List<Pessoa> lista = new ArrayList<Pessoa>();
		lista.add(new Pessoa("Daniel", "Analista"));
		lista.add(new Pessoa("Daniela", "Programadora"));
		model.addAttribute("pessoas", lista);
		return "lista";
	}

	@RequestMapping(value ="/salvar", method = RequestMethod.POST)
	public String salvar(Pessoa pessoa) {
		System.out.println("Pessoa " + pessoa.getNome() + " cadastrada!");
		return "redirect:/listar";
	}
}